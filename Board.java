public class Board {
    private Square[][] tictactoeBoard;
	
	//i=row,j=column
	public Board() {
        this.tictactoeBoard = new Square[3][3];
        for (int i=0; i<this.tictactoeBoard.length; i++){
            for (int j=0;j<this.tictactoeBoard.length; j++){
                this.tictactoeBoard[i][j] = Square.BLANK;
            }
        }
    }

	public String toString() {
    String str ="";
	for (int i=0; i<this.tictactoeBoard.length; i++){
		for (int j= 0; j< this.tictactoeBoard.length; j++){
            str+=this.tictactoeBoard[i][j] + " ";
		}
		str+= "\n";
	}
    return str;
	}

	public boolean placeToken(int row, int col, Square playerToken) {
		// not in a current range return false
		if (row<0 || row>2 || col<0 || col>2) {
			return false;
		}

		if (this.tictactoeBoard[row][col].equals(Square.BLANK)){
			this.tictactoeBoard[row][col] = playerToken;
			return true;
		}
		else{
        return false;
		}
	}
	
	public boolean checkIfFull(){
		for (int row=0; row<this.tictactoeBoard.length; row++){
			for (int col=0; col<this.tictactoeBoard.length;col++){
				if (tictactoeBoard[row][col].equals(Square.BLANK)) {
                return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken) {
		for (int row=0; row<this.tictactoeBoard.length;row++) {
			boolean allMatch = true;
			for (int col=0;col<this.tictactoeBoard.length; col++) {
				if (this.tictactoeBoard[row][col]!= playerToken) {
					allMatch= false;
				}
			}
			
			if(allMatch) {
				return true;
			}
		}	
		return false;
	}

	private boolean checkIfWinningVertical(Square playerToken) {
		for (int col = 0; col<this.tictactoeBoard.length; col++) {
			boolean allMatch = true;
			for (int row = 0; row<this.tictactoeBoard.length; row++) {
				if (this.tictactoeBoard[row][col]!= playerToken) {
                allMatch = false;
				}
			}
			
			if (allMatch){
            return true;
			}
		}
		return false;
	}


	public boolean checkIfWinning(Square playerToken) {
		if (checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken)){
			return true;
		}
		else {
			return false;
		}
	}
  
}
