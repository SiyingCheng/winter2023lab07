import java.util.Scanner;
public class TicTacToeGame {
    public static void main(String[] args) {
        System.out.println("Welcome to TicTacToe Game!");

        Board board = new Board();
        boolean gameOver = false;
        int player = 1;
        Square playerToken = Square.X;
		
		while(!gameOver){
        
		System.out.println(board);

        if (player==1) {
            playerToken = Square.X;
			System.out.println("Player "+player);
        } 
		else {
            playerToken = Square.O;
			System.out.println("Player "+player);
        }
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter row(interger): ");
		int row = scanner.nextInt();
		System.out.print("Enter column(interger): ");
		int col = scanner.nextInt();
		
		// check the player input 
		if(!board.placeToken(row, col, playerToken)) {
			System.out.println("Invalid value.Please enter row and column again in between 0-2 or enter a value that is not repeated: ");
			System.out.print("Enter row again: ");
			row = scanner.nextInt();
			System.out.print("Enter column again: ");
			col = scanner.nextInt();
		}
		
		if(board.checkIfWinning(playerToken)){
			System.out.println("Player " + player + " is the WINNER!");
			gameOver = true;
		}
		else if (board.checkIfFull()) {
        System.out.println("It's a tie!");
        gameOver = true;
		}
		else {
        player = player+1;
			if (player > 2) {
				player = 1;
			}
		}
	}
	
	
}
}